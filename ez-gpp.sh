#!/bin/bash

rxHelp="-{0,2}h(elp)?"
rxInstall="-{0,2}i(n|nstall)?"
rxBuild="-{0,2}b(ui|uild)?"
rxCommit="-{0,2}c(o|ommit)?"
rxCommand="(object|plugin|state)"
arg="$1"
arg2="$2"
mod="./node_modules/.bin/"

if [[ -z $arg || $arg =~ $rxHelp ]]; then
	echo "Easy Generator-Phaser-Plus"
	echo "               Version 3.1"
	echo
	echo "Usage: ./ez-gpp.sh [OPTION]"
	echo "OPTIONS:"
	echo "  -h --help       Show this help"
	echo "  -i --install    Install Generator"
	echo "  -b --build      Build the Game (in the 'dist' folder)"
	echo "  -c --commit     Add changes, commit and push"
	echo "  object          Add an Object/Prefab"
	echo "  plugin          Add a Plugin"
	echo "  state           Add a State"
	exit
elif [[ $arg =~ $rxInstall ]]; then
	echo "Installing and running Generator-Phaser-Plus...";
	#mkdir $arg && cd $_;
	npm install yo generator-phaser-plus;
	$mod"/yo" phaser-plus;
elif [[ $arg =~ $rxCommit ]]; then
	echo "WUUUUT";
	git add -u && git commit && git push;
elif [[ $arg =~ $rxCommand ]]; then
	echo "Runnning $arg Sub-Generator...";
	$mod"/yo" phaser-plus:$arg;
elif [[ $arg =~ $rxBuild ]]; then
	npm run dist;
else
	echo "Unrecognized command: \"$arg\""
fi
