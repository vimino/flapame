import assets from '../assets';

export default class Preload extends Phaser.State {
  preload() {
    // Load the Game assets
    this.load.pack('game', null, assets);

    // Get the Center of the Screen
    const {centerX: x, centerY: y} = this.world;
    // Add the Preloader bar
    const preloader = this.game.add.sprite(0, 0, 'preloader');
    // Center the Preloader
    preloader.x = x - preloader.width/2;
    preloader.y = y - preloader.height/2;
    // Animate the Preloader
    preloader.animations.add('spin');
    preloader.animations.play('spin', 20, true);
  }

  create() {
    // Once loader, switch to the Game
    this.state.start('Menu');
  }
}
