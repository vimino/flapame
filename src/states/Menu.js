import Ground from '../objects/Ground';
//import Bird from '../objects/Bird';

export default class Menu extends Phaser.State {
  create() {
    // Set the global variables
    if(!('opt' in this.game) || typeof this.game.opt === 'undefined') {
      this.game.opt = {
        sprite: 0,
        ghost: false,
        ghostTime: 1000,
        mute: false,
        full: false,
        debug: false
      };
    }
    const opts = this.game.opt;

    // Get the screen center and size
    const {centerX: cx, width: ww, height: wh} = this.world;

    // Add the background (moving slow)
    const background = this.add.tileSprite(0, 0, 288, 505, 'background');
    background.autoScroll(-5, 0);

    // Add the ground (moving fast)
    const ground = new Ground(this.game, 0, 400, 335, 112);
    this.add.existing(ground);

    // Create the Title Group
    const titleGroup = this.add.group();
    titleGroup.x = 48;
    titleGroup.y = 100;
    this.add.tween(titleGroup).to({y: 115}, 350, Phaser.Easing.Linear.NONE, true, 0, 1000, true);

    const tts = this.game.add.bitmapText(0, -40, 'flappyfont', 'tap to switch bird', 12);
    titleGroup.add(tts);

    const pointer = this.add.sprite(0, -20, 'pointer', 0);
    titleGroup.add(pointer);

    const title = this.add.sprite(0, 0, 'title', 0);
    if(opts.sprite > 0) {
      title.frame = opts.sprite;
    }
    titleGroup.add(title);

    const htw = title.width/2;
    tts.x = htw - tts.width/2;
    pointer.x = htw - pointer.width/2;

    const mode = this.game.add.bitmapText(0, 70, 'flappyfont', 'Flappy', 24);
    if(opts.sprite === 0) {
      mode.tint = 0xccccff;
      mode.text = 'Flappy';
    } else if(opts.sprite === 1) {
      mode.tint = 0xccffcc;
      mode.text = 'Smooth';
    } else if(opts.sprite === 2) {
      mode.tint = 0xffffcc;
      mode.text = 'Groovy';
    } else if(opts.sprite === 3) {
      mode.tint = 0xffcccc;
      mode.text = 'Crazy';
    } else if(opts.sprite === 4) {
      mode.tint = 0xddbbff;
      mode.text = 'Anonymous';
    }
    mode.x = htw - mode.width/2;
    titleGroup.add(mode);

    title.mode = mode;
    title.inputEnabled = true;
    title.events.onInputDown.add(this.swapSprite,title);

    mode.title = title;
    mode.inputEnabled = true;
    mode.events.onInputDown.add(this.swapSprite,mode);

    //const bird = new Bird(this.game, 215, 20);
    // if(opts.ghost === true) {
    //   bird.alpha = 0.5;
    // }
    //titleGroup.add(bird);
    // bird.inputEnabled = true;
    // bird.events.onInputDown.add(this.toggleGhost,bird);

    // Add the Buttons (and specify their Actions)
    const startButton = this.add.button(cx, 300, 'start-button', this.playGame, this);
    startButton.anchor.setTo(0.5, 0.5);

    // Add a button to mute all sounds
    const muteButton = this.add.button(cx - 24, 360, 'options', this.toggleMute, muteButton);
    muteButton.anchor.setTo(0.5, 0.5);
    if(opts.mute === true) {
      muteButton.frame = 3;
    } else {
      muteButton.frame = 2;
    }

    // Add a button to toggle fullscreen mode
    const fullscreenButton = this.game.add.button(cx + 24, 360, 'options', this.toggleFullscreen, fullscreenButton);
    fullscreenButton.anchor.setTo(0.5, 0.5);
    if(opts.full === true) {
      fullscreenButton.frame = 1;
    } else {
      fullscreenButton.frame = 0;
    }

    // In the case of Fullscreen, we must consider external changes
    this.scale.onFullScreenChange.add(this.fullscreenChange, fullscreenButton);

    // Place the Version label on the lower-right corner
    const versionText = this.game.add.bitmapText(ww, wh, 'flappyfont', '1.2.0', 18);
    versionText.x = ww - versionText.width - 16;
    versionText.y = wh - versionText.height - 10;
    if(opts.debug === true) {
      versionText.tint = 0x00ff00;
    }
    versionText.inputEnabled = true;
    versionText.events.onInputDown.add(this.toggleDebug, versionText);

    // Place the Logo on the lower-right corner
    const mini = this.add.button(ww, wh, 'mini-vimo', this.goHome, mini);
    mini.x -= mini.width - 20;
    mini.y -= mini.height;
    mini.anchor.setTo(0.5, 0.5);
    this.game.add.existing(mini);

    const spaceBar = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    spaceBar.onDown.add(this.playGame, this);
  }

  // Switch to the Play state
  playGame() {
    // this.game.opt.offsetBack = this.background.tilePosition.x;
    // this.game.opt.offsetGround = this.ground.getOffset();
    this.state.start('Play');
  }

  // Toggle bird sprite
  swapSprite() {
    const opts = this.game.opt;
    opts.sprite = opts.sprite + 1;
    if(opts.sprite > 4) {
      opts.sprite = 0;
    }
    let title = this.title;
    if(typeof title === 'undefined') {
      title = this;
    }
    title.frame = opts.sprite;

    let mode = this.mode;
    if(typeof mode === 'undefined') {
      mode = this;
    }
    if(opts.sprite === 0) {
      mode.tint = 0xccccff;
      mode.text = 'Flappy';
    } else if(opts.sprite === 1) {
      mode.tint = 0xccffcc;
      mode.text = 'Smooth';
    } else if(opts.sprite === 2) {
      mode.tint = 0xffffcc;
      mode.text = 'Groovy';
    } else if(opts.sprite === 3) {
      mode.tint = 0xffcccc;
      mode.text = 'Crazy';
    } else if(opts.sprite === 4) {
      mode.tint = 0xddbbff;
      mode.text = 'Anonymous';
    }
    mode.x = title.width/2 - mode.width/2;
  }

  // Toggle the physical body (transparent when disabled)
  // toggleGhost() {
  //   const opts = this.game.opt;
  //   opts.ghost = !opts.ghost;
  //   if(opts.ghost) {
  //     this.alpha = 0.5;
  //   } else {
  //     this.alpha = 1.0;
  //   }
  // }

  // Toggle the game sounds
  toggleMute() {
    const opts = this.game.opt;
    opts.mute = !opts.mute;
    if(opts.mute) {
      this.frame = 3;
    } else {
      this.frame = 2;
    }
  }

  // Toggle fullscreen and change the button accordingly
  toggleFullscreen() {
    const opts = this.game.opt;
    opts.full = !opts.full;
    if(opts.full) {
      const success = this.game.scale.startFullScreen(false);
      if(success === true) {
        this.frame = 1;
      }
    } else {
      this.frame = 0;
      this.game.scale.stopFullScreen();
    }
  }

  // Follow a link back to the source
  goHome() {
    // console.log(this.game.net.getHostName());
    this.game.net.updateQueryString('', '', true, 'https://www.vimino.net/watch/projects/flapa-me');
  }

  // Toggles debug mode (shows the bird collision body)
  toggleDebug() {
    const opts = this.game.opt;
    opts.debug = !opts.debug;
    if(opts.debug) {
      this.tint = 0x00ff00;
    } else {
      this.tint = 0xffffff;
    }
  }

  // This function only needs to change the button state
  fullscreenChange(event) {
    const enabled = event.isFullScreen;
    // Update the fullscreenButton as well
    if(enabled === true) {
      this.frame = 1;
    } else {
      this.frame = 0;
    }
  }
}
