import Ground from '../objects/Ground';
import Bird from '../objects/Bird';
import Pipe from '../objects/Pipe';
import PipeGroup from '../objects/PipeGroup';
import Scoreboard from '../objects/Scoreboard';

export default class Game extends Phaser.State {
  constructor() {
    super();
    this.background = undefined;
    this.ground = undefined;
    this.bird = undefined;
    this.instructionGroup = undefined;
    this.score = 0;
    this.scoreText = undefined;
    this.pipes = undefined;
    this.pipeGenerator = undefined;
    this.gameover = false;
    this.scoreboard = undefined;
  }
  create() {
    const opts = this.game.opt;
    this.ghost = false;
    this.ghostStart = 0;
    this.lives = 3;

    // Get the screen center
    const {centerX: cx, centerY: cy} = this.world;

    // Initialize the physics and gravity
    this.physics.startSystem(Phaser.Physics.ARCADE);
    let gravity = 1200;
    if(opts.sprite === 1) {
      gravity = 800;
    }
    this.game.physics.arcade.gravity.y = gravity;

    // Add the background (moving slow)
    this.background = this.add.tileSprite(0, 0, 288, 505, 'background');
    this.background.autoScroll(-5, 0);

    // Add the Bird
    this.bird = new Bird(this.game, 100, cy, opts.sprite);
    this.add.existing(this.bird);

    // Add the ground (moving fast)
    let speed = 200;
    if(opts.sprite === 1) {
      speed = 150;
    }
    this.ground = new Ground(this.game, 0, 400, 335, 112, speed);
    this.add.existing(this.ground);

    // Make a group for the pairs of pipes
    this.pipes = this.add.group();

    this.score = 0;
    this.scoreText = this.add.bitmapText(cx, 10, 'flappyfont', this.score.toString(), 24);
    this.scoreText.visible = false;

    this.feather = this.add.sprite(4, 4, 'feather');
    if(opts.sprite === 0) {
      this.feather.tint = 0xbbbbff;
    } else if(opts.sprite === 1) {
      this.feather.tint = 0xbbffbb;
    } else if(opts.sprite === 2) {
      this.feather.tint = 0xffffbb;
    } else if(opts.sprite === 3) {
      this.feather.tint = 0xffbbbb;
    } else if(opts.sprite === 4) {
      this.feather.tint = 0xbb88ff;
    }
    this.feather.visible = false;

    this.livesText = this.add.bitmapText(28, 17, 'flappyfont', this.lives.toString(), 16);
    this.livesText.visible = false;

    this.instructionGroup = this.add.group();
    this.instructionGroup.add(this.add.sprite(cx, 100, 'get-ready'));
    this.instructionGroup.add(this.add.sprite(cx, 325, 'instructions'));
    this.instructionGroup.setAll('anchor.x', 0.5);
    this.instructionGroup.setAll('anchor.y', 0.5);

    this.gameover = false;

    // Input
    const flapKey = this.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    flapKey.onDown.addOnce(this.startGame, this);
    flapKey.onDown.add(this.bird.flap, this.bird);

    this.game.input.onDown.addOnce(this.startGame, this);
    this.game.input.onDown.add(this.bird.flap, this.bird);

    // Sounds
    this.scoreSound = this.add.audio('score');
    this.ouchSound = this.add.audio('ouch');
    this.pipeHitSound = this.add.audio('pipeHit');
    this.groundHitSound = this.add.audio('groundHit');
  }

  update() {
    if(this.gameover === false) {
      this.game.physics.arcade.collide(this.bird, this.ground, this.deathHandler, null, this);

      this.pipes.forEach((pipeGroup) => {
        this.checkScore(pipeGroup);
        if(this.ghost === false) {
          this.game.physics.arcade.collide(this.bird, pipeGroup, this.deathHandler, null, this);
        } else {
          const timeLeft = (this.game.time.totalElapsedSeconds() - (this.ghostStart + 2));
          if(timeLeft >= 0) {
            this.getReal();
          }
        }
      }, this);
    }
  }

  render() {
    if(this.game.opt.debug === true) {
      this.game.debug.body(this.bird);
    }
  }

  shutdown() {
    this.bird.destroy();
    if(typeof this.pipes !== 'undefined') {
      this.pipes.destroy();
    }
    if(typeof this.scoreboard !== 'undefined') {
      this.scoreboard.destroy();
    }
  }

  startGame() {
    const opts = this.game.opt;

    if(this.bird.alive === false && this.gameover === false) {
      this.bird.body.allowGravity = true;
      this.bird.alive = true;

      let interval = 1.25;
      if(opts.sprite === 1) {
        interval = 1.75;
      } else if(opts.sprite === 2) {
        interval = 1;
      }
      this.pipeGenerator = this.game.time.events.loop(Phaser.Timer.SECOND * interval, this.generatePipes, this);
      this.pipeGenerator.timer.start();

      this.instructionGroup.destroy();
      this.scoreText.visible = true;

      this.feather.visible = true;
      this.livesText.visible = true;
    }
  }

  checkScore(pipeGroup) {
    const opts = this.game.opt;

    if(pipeGroup.exists &&
      pipeGroup.hasScored === false &&
      pipeGroup.topPipe.world.x <= this.bird.world.x &&
      this.ghost === false) {
      pipeGroup.hasScored = true;
      this.score++;
      this.scoreText.setText(this.score.toString());
      if((opts.sprite === 1 || opts.sprite === 2) && Math.random() > 0.95) {
        this.lives++;
        if(this.lives === 2) {
          this.bird.tint = 0xffbbbb;
        } else if (this.lives === 1) {
          this.bird.tint = 0xff7777;
        } else if (this.lives === 0) {
          this.bird.tint = 0xff3333;
        }
      }
      this.livesText.setText(this.lives.toString());
      if(typeof this.scoreSound !== 'undefined' && typeof this.scoreSound.play !== 'undefined' && opts.mute === false) {
        this.scoreSound.play();
      }
    }
  }

  deathHandler(bird, enemy) {
    if(enemy instanceof Pipe) {
      if(this.lives > 0) {
        this.game.playSound(this.ouchSound);
        this.lives--;
        this.livesText.text = this.lives.toString();
        this.getGhosted();
        return;
      }

      this.game.playSound(this.pipeHitSound);
    } else {
      this.game.playSound(this.groundHitSound);
    }

    if(this.gameover === false) {
      this.gameover = true;
      this.pipes.callAll('stop');
      if(typeof this.pipeGenerator !== 'undefined') {
        this.pipeGenerator.timer.stop();
      }
      this.bird.kill();
      this.background.stopScroll();
      this.ground.stopScroll();
      this.scoreText.visible = false;

      this.scoreboard = new Scoreboard(this.game);
      this.game.add.existing(this.scoreboard);
      this.scoreboard.show(this.score);
    }
  }

  generatePipes() {
    const opts = this.game.opt;
    let range = 100;
    if(opts.sprite === 2) {
      range = 50;
    }
    const pipeY = this.game.rnd.integerInRange(-range,range);
    let pipeGroup = this.pipes.getFirstExists(false);
    if(!pipeGroup) {
      let speed = 200;
      if(opts.sprite === 1) {
        speed = 150;
      }
      pipeGroup = new PipeGroup(this.game, speed, this.pipes);
    }
    pipeGroup.reset(this.game.width, pipeY);
  }

  getGhosted() {
    this.ghost = true;
    this.ghostStart = this.game.time.totalElapsedSeconds();
    if(this.lives === 2) {
      this.bird.tint = 0xffbbbb;
    } else if (this.lives === 1) {
      this.bird.tint = 0xff7777;
    } else if (this.lives === 0) {
      this.bird.tint = 0xff3333;
    }
    this.bird.alpha = 0.3;
  }

  getReal() {
    this.ghost = false;
    this.ghostStart = 0;
    this.bird.alpha = 1.0;
  }
}
