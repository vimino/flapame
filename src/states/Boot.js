import assets from '../assets';

export default class Boot extends Phaser.State {
  preload() {
    this.load.path = 'assets/';

    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;
    this.input.maxPointers = 1;

    // Crisp graphics. Great for pixel-art!
    //Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);
    //this.stage.smoothed = false;

    // If the game canvas loses focus, keep the game loop running.
    this.stage.disableVisibilityChange = false;

    // Set the background color
    this.stage.backgroundColor = '#078cfd';

    this.game.playSound = (sound) => {
      if(typeof sound !== 'undefined' &&
      sound.play !== 'undefined' &&
      this.game.opt.mute === false) {
        sound.play();
      }
    };

    // In the case of Fullscreen, we must consider external changes
    this.game.scale.onFullScreenChange.add(this.fullscreenChange, this.game);

    // If back button is pushed (mobile phone\tablet), go to Menu
    document.addEventListener('backbutton', () => {
      this.game.state.start('Menu');
    }, false);

    this.load.pack('boot', null, assets);
  }

  create() {
    this.state.start('Preload');
  }

  // We need this function to have access to game.opt globally
  fullscreenChange(event) {
    const opts = this.opt;
    const enabled = event.isFullScreen;
    // Change the "full" state acording to the current one
    opts.full = enabled;
  }
}
