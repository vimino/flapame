export default class Scoreboard extends Phaser.Group {
  constructor(game, ...args) {
    super(game, ...args);
    const opts = this.game.opt;

    // Get the Center of the Screen
    const {centerX: cx} = game.world;

    // Add the "Game Over" label
    const gameover = this.create(cx, 100, 'gameover');
    gameover.anchor.setTo(0.5, 0.5);

    // Add the Scoreboard (background image)
    this.scoreboard = this.create(cx, 200, 'scoreboard');
    this.scoreboard.anchor.setTo(0.5, 0.5);

    // Add the Score text
    this.scoreText = this.game.add.bitmapText(this.scoreboard.width + 4, 181, 'flappyfont', '', 18);
    this.add(this.scoreText);

    // Add the Best Score text
    this.bestText = this.game.add.bitmapText(this.scoreboard.width + 4, 230, 'flappyfont', '', 18);
    this.add(this.bestText);

    let tint = 0xffffff;
    if(opts.sprite === 0) {
      tint = 0xccccff;
    } else if(opts.sprite === 1) {
      tint = 0xccffcc;
    } else if(opts.sprite === 2) {
      tint = 0xffffcc;
    } else if(opts.sprite === 3) {
      tint = 0xffcccc;
    } else if(opts.sprite === 4) {
      tint = 0xddbbff;
    }
    this.scoreText.tint = tint;
    this.bestText.tint = tint;

    if(opts.debug === true) {
      this.bestText.alpha = 0.5;
    }

    // Add a button to return to the Menu
    const backButton = this.game.add.button(cx - 52, 300, 'back-button', this.returnToMenu, backButton);
    backButton.anchor.setTo(0.5, 0.5);
    this.add(backButton);

    // Add a start button to play again
    const startButton = this.game.add.button(cx + 32, 300, 'start-button', this.playGame, startButton);
    startButton.anchor.setTo(0.5, 0.5);
    this.add(startButton);

    // Hide everything under the bottom
    this.y = this.game.height;
    this.x = 0;

    // Input
    const keySpacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    keySpacebar.onDown.add(this.playGame, this);
    const keyBackspace =  this.game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
    keyBackspace.onDown.add(this.returnToMenu, this);
  }

  // Switch to the Menu state
  returnToMenu() {
    this.game.state.start('Menu');
  }

  // Switch to the Play state
  playGame() {
    this.game.state.start('Play');
  }

  // Set and align the text to the right
  setAlignedText(label, score) {
    const str = score.toString();
    label.setText(str);
    label.x = 242 - label.width;
  }

  // Slide the Scoreboard group up
  show(score) {
    const opts = this.game.opt;
    let bestScore, value, medal;

    // Update the score and align it
    this.setAlignedText(this.scoreText, score);

    // Update the best score if available
    try {
      let itemLabel = 'BestScore'+opts.sprite;
      if(typeof localStorage !== 'undefined') {
        bestScore = localStorage.getItem(itemLabel);
        if(typeof bestScore === 'undefined' ||
          bestScore === null ||
          parseInt(bestScore) < parseInt(score)) {
          bestScore = score;
          if(opts.debug === false) {
            localStorage.setItem(itemLabel, bestScore);
          }
        }
      }
    } catch(exception) {
      // ignore the bestScore if localStorage isn't allowed
    }

    // Update the best score and align it (if there is one)
    if(typeof bestScore === 'undefined') {
      this.bestText.visible = false;
    } else {
      this.setAlignedText(this.bestText, bestScore);
    }

    // Add the "slide up" animation
    this.game.add.tween(this).to({y: 0}, 1000, Phaser.Easing.Bounce.Out, true);

    // Check if the score is worth a medal
    let diff = 20;
    if(opts.sprite === 1 || opts.sprite === 2) {
      diff = 50;
    } else if(opts.sprite === 3) {
      diff = 10;
    }
    if(score >= diff && score < diff*2) {
      value = 1;
    } else if(score >= diff*2) {
      value = 0;
    }

    // If it is worth a medal
    if(typeof value !== 'undefined') {
      // Add the Medal sprite
      medal = this.game.add.sprite(-65, 7, 'medals', value);
      medal.anchor.setTo(0.5, 0.5);
      this.scoreboard.addChild(medal);

      // Add a Particle Effect (medal glow)
      const emitter = this.game.add.emitter(medal.x, medal.y, 400);
      emitter.width = medal.width;
      emitter.height = medal.height;
      this.scoreboard.addChild(emitter);

      // Animate the particles
      emitter.makeParticles('particle');
      emitter.setRotation(-100, 100);
      emitter.setXSpeed(0, 0);
      emitter.setYSpeed(0, 0);
      emitter.minParticleScale = 0.25;
      emitter.maxParticleScale = 0.5;
      emitter.setAll('body.allowGravity', false);
      emitter.start(false, 1000, 1000);
    }
  }
}
