export default class Ground extends Phaser.Group {
  constructor(game, x, y, width, height, speed = 200) {
    super(game);

    // Add the (static) bottom
    Ground.bottom = game.add.sprite(x, y, 'ground-bottom');

    const topHeight = height - Ground.bottom.height;
    Ground.bottom.y = y + topHeight;

    // Add the (moving) Top
    // Since its used between functions it must be an instance
    Ground.top = game.add.tileSprite(x, y, width, topHeight, 'ground-top');
    // Scroll it to the left
    Ground.top.autoScroll(-1 * speed, 0);

    this.add(Ground.bottom);
    this.add(Ground.top);

    // If playing, enable the physics for top
    if(game.state.current === 'Play') {
      game.physics.arcade.enableBody(Ground.top);
      game.physics.arcade.enableBody(Ground.bottom);
      this.setAll('body.allowGravity', false);
      this.setAll('body.immovable', true);
    }
  }

  stopScroll() {
    Ground.top.stopScroll();
  }
}
