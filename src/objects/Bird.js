export default class Bird extends Phaser.Sprite {
  constructor(game, x, y, sprite = 0) {
    if(sprite == 1) {
      super(game, x, y, 'cool');
    } else if(sprite == 2) {
      super(game, x, y, 'groovy');
    } else if(sprite == 3) {
      super(game, x, y, 'crazy');
    } else if(sprite == 4) {
      super(game, x, y, 'anonymous');
    } else {
      super(game, x, y, 'bird');
    }
    this.anchor.setTo(0.5, 0.5);
    this.sprite = sprite;

    // Animations
    this.animations.add('flap');
    if(sprite === 1) {
      this.animations.play('flap', 8, true);
    } else if(sprite === 3) {
      this.animations.play('flap', 24, true);
    } else {
      this.animations.play('flap', 12, true);
    }

    // If playing activate Physics
    if(game.state.current === 'Play') {
      // Physics
      this.game.physics.arcade.enable(this);
      this.body.collideWorldBounds = true;
      if(this.width > this.height) {
        this.body.setCircle(this.height/2);
      } else {
        this.body.setCircle(this.width/2);
      }

      // Sounds
      this.flapSound = this.game.add.audio('flap');

      // Wait for user
      this.alive = false;
      this.body.allowGravity = false;
    }
  }

  update() {
    if(this.game.state.current === 'Play') {
      if(this.angle < 90 && this.alive) {
        this.angle += 2.5;
      }

      if(!this.alive) {
        this.body.velocity.x = 0;
      }
    }
  }

  flap() {
    // Make sure the Bird is alive
    if(this.alive === false) {
      return;
    }

    // Play "flap" sound
    if(typeof this.flapSound !== 'undefined' && typeof this.flapSound.play !== 'undefined' && this.game.opt.mute === false) {
      this.flapSound.play();
    }

    // Move higher
    if(this.sprite === 1) {
      this.body.velocity.y = -300;
    } else if(this.sprite === 2) {
      this.body.velocity.y = -350;
    } else if(this.sprite === 3) {
      let crazyY = -Math.floor(Math.random() * 500);
      this.body.velocity.y = crazyY;
      let crazyX = Math.floor(Math.random() * 100) - 50;
      this.body.velocity.x = crazyX;
    } else {
      this.body.velocity.y = -400;
    }

    // Rotate upwards
    this.game.add.tween(this).to({angle: -40}, 100).start();
  }
}
