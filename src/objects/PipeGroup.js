import Pipe from './Pipe';

export default class PipeGroup extends Phaser.Group {
  constructor(game, speed, ...args) {
    super(game, ...args);
    this.topPipe = new Pipe(this.game, 0, 0, 0);
    this.bottomPipe = new Pipe(this.game, 0, 440, 1);

    this.add(this.topPipe);
    this.add(this.bottomPipe);

    this.hasScored = false;
    this.setAll('body.velocity.x', -speed);
    this.speed = speed;
  }

  reset(x, y) {
    this.topPipe.reset(0, 0);
    this.bottomPipe.reset(0, 440);
    this.x = x;
    this.y = y;
    this.setAll('body.velocity.x', -this.speed);
    this.hasScored = false;
    this.exists = true;
  }

  checkWorldBounds() {
    if(!this.topPipe.inWorld) {
      this.exists = false;
    }
  }

  update() {
    this.checkWorldBounds();
  }
}
