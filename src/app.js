import 'babel-polyfill';

import * as states from './states';

export function init() {
  const game = new Phaser.Game(288, 505, Phaser.AUTO);

  // Dynamically add all required game states.
  Object
    .entries(states)
    .forEach(([key, state]) => game.state.add(key, state));

  game.state.start('Boot');

  return game;
}
