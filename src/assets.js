export default {
  boot: [{
    key: 'preloader',
    type: 'spritesheet',
    frameMax: 10,
    frameWidth: 210,
    frameHeight: 15
  }],

  game: [{
    key: 'background',
    type: 'image'
  },{
    key: 'ground-top',
    type: 'image'
  },{
    key: 'ground-bottom',
    type: 'image'
  },{
    key: 'pointer',
    type: 'image'
  },{
    key: 'title',
    type: 'spritesheet',
    frameMax: 5,
    frameWidth: 192,
    frameHeight: 64
  },{
    key: 'start-button',
    type: 'image'
  },{
    key: 'options',
    type: 'spritesheet',
    frameMax: 4,
    frameWidth: 32,
    frameHeight: 32
  },{
    key: 'bird',
    type: 'spritesheet',
    frameMax: 3,
    frameWidth: 34,
    frameHeight: 24
  },{
    key: 'cool',
    type: 'spritesheet',
    frameMax: 3,
    frameWidth: 34,
    frameHeight: 24
  },{
    key: 'groovy',
    type: 'spritesheet',
    frameMax: 3,
    frameWidth: 36,
    frameHeight: 24
  },{
    key: 'crazy',
    type: 'spritesheet',
    frameMax: 3,
    frameWidth: 38,
    frameHeight: 24
  },{
    key: 'anonymous',
    type: 'spritesheet',
    frameMax: 3,
    frameWidth: 48,
    frameHeight: 36
  },{
    key: 'medals',
    type: 'spritesheet',
    frameMax: 2,
    frameWidth: 44,
    frameHeight: 46
  },{
    key: 'pipes',
    type: 'spritesheet',
    frameMax: 2,
    frameWidth: 54,
    frameHeight: 320
  },{
    key: 'flappyfont',
    type: 'bitmapFont',
    atlasURL: 'flappyfont/flappyfont.fnt',
    textureURL: 'flappyfont/flappyfont.png'
  },{
    key: 'get-ready',
    type: 'image'
  },{
    key: 'gameover',
    type: 'image'
  },{
    key: 'scoreboard',
    type: 'image'
  },{
    key: 'instructions',
    type: 'image'
  },{
    key: 'particle',
    type: 'image'
  },{
    key: 'back-button',
    type: 'image'
  },{
    key: 'mini-vimo',
    type: 'image'
  },{
    key: 'feather',
    type: 'image'
  },{
    key: 'flap',
    type: 'audio',
    urls: ['flap.wav']
  },{
    key: 'groundHit',
    type: 'audio',
    urls: ['ground-hit.wav']
  },{
    key: 'ouch',
    type: 'audio',
    urls: ['ouch.wav']
  },{
    key: 'pipeHit',
    type: 'audio',
    urls: ['pipe-hit.wav']
  },{
    key: 'score',
    type: 'audio',
    urls: ['score.wav']
  }]
};

  // }, {
  //   //  Example: Add an image.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'image',
  //   url: 'example.png'
  // }, {
  //   //  Example: Add a text file.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.txt` is assumed.
  //   //
  //   //  Retrieve the file with `game.cache.getText(<key>)`.
  //   key: 'example',
  //   type: 'text',
  //   url: 'example.txt'
  // }, {
  //   //  Example: Add a JSON document.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.json` is assumed.
  //   //
  //   //  Retrieve the file with `game.cache.getJSON(<key>)`.
  //   key: 'example',
  //   type: 'json',
  //   url: 'example.json'
  // }, {
  //   //  Example: Add a XML document.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.xml` is assumed.
  //   //
  //   //  Retrieve the file with `game.cache.getXML(<key>)`.
  //   key: 'example',
  //   type: 'xml',
  //   url: 'example.xml'
  // }, {
  //   //  Example: Add a custom format, binary file.
  //   //
  //   //  The `url` is mandatory. Requires a callback context.
  //   //
  //   //  Retrieve the file with `game.cache.getBinary(<key>)`.
  //   key: 'example',
  //   type: 'binary',
  //   url: 'example.bin',
  //   callback: 'exampleCallback'
  // }, {
  //   //  Example: Add a spritesheet texture.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'spritesheet',
  //   url: 'example.png',
  //   margin: 0,
  //   spacing: 0,
  //   frameMax: 8,
  //   frameWidth: 32,
  //   frameHeight: 32
  // }, {
  //   //  Example: Add video.
  //   //
  //   //  Supply `urls` for one of several files in different formats.
  //   key: 'example',
  //   type: 'video',
  //   urls: ['example.m4v', 'example.webm']
  // }, {
  //   //  Example: Add audio.
  //   //
  //   //  Supply `urls` for one of several files in different formats.
  //   key: 'example',
  //   type: 'audio',
  //   urls: ['example.m4a', 'example.oga']
  // }, {
  //   //  Example: Add an audio sprite with some sound effects.
  //   //
  //   //  Supply `urls` for one of several files in different formats.
  //   //
  //   //  The mandatory `jsonURL` field specifies the audio sprites data.
  //   key: 'example',
  //   type: 'audiosprite',
  //   urls: ['example.m4a', 'example.oga'],
  //   jsonURL: 'example.json'
  // }, {
  //   //  Example: Add a Tiled tilemap.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.csv` or `<key>.json` is assumed.
  //   //
  //   //  The `format` field specifies in which data format your tilemap was
  //   //  exported, either `CSV` or `TILED_JSON`.
  //   //
  //   //  Use different `image` pack entries to load the necessary textures.
  //   key: 'example',
  //   type: 'tilemap',
  //   url: 'example.json',
  //   format: 'TILED_JSON'
  // }, {
  //   //  Example: Add a Lime+Corona physics data file.
  //   //
  //   //  If `url` is omitted, a pattern `<key>.json` is assumed.
  //   key: 'example',
  //   type: 'physics',
  //   url: 'example.json'
  // }, {
  //   //  Example: Add a retro, bitmap font.
  //   //
  //   //  If `atlasURL` is omitted, a pattern `<key>.json` is assumed.
  //   //
  //   //  If `textureURL` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'bitmapFont',
  //   atlasURL: 'example.json',
  //   textureURL: 'example.png',
  //   xSpacing: 0,
  //   ySpacing: 0
  // }, {
  //   //  Example: Add a texture atlas.
  //   //
  //   //  Use the `format` field to specify the texture atlas data format:
  //   //  - `TEXTURE_ATLAS_XML_STARLING`: Starling XML data format.
  //   //  - `TEXTURE_ATLAS_JSON_HASH`: JSON Hash data format.
  //   //  - `TEXTURE_ATLAS_JSON_ARRAY`: JSON Array data format (default).
  //   //
  //   //  If `atlasURL` is omitted, a pattern `<key>.json` (or `<key>.xml`) is
  //   //  assumed.
  //   //
  //   //  If `textureURL` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'atlas',
  //   atlasURL: 'example.json',
  //   textureURL: 'example.png',
  //   format: 'TEXTURE_ATLAS_JSON_HASH'
  // }, {
  //   //  Example: Add a texture atlas (alternative form).
  //   //
  //   //  Use the `type` field to specify the texture atlas format, as follows:
  //   //  - `atlasXML`: Starling XML Texture Atlas data format.
  //   //  - `atlasJSONHash`: texture atlas in JSON Hash data format.
  //   //  - `atlasJSONArray`: texture atlas in JSON Array data format.
  //   //
  //   //  If `atlasURL` is omitted, a pattern `<key>.json` (or `<key>.xml`) is
  //   //  assumed.
  //   //
  //   //  If `textureURL` is omitted, a pattern `<key>.png` is assumed.
  //   key: 'example',
  //   type: 'atlasJSONHash',
  //   atlasURL: 'example.json',
  //   textureURL: 'example.png'
