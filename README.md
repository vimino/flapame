## Flapa-me
### Yet another FlappyBird Reborn clone
![Build Status](https://gitlab.com/vimino/flapame/badges/master/build.svg)

A while ago I followed this [Codevinsky tutorial](http://www.codevinsky.com/phaser-2-0-tutorial-flappy-bird-part-1/) and created my own "Flappy Bird Reborn", adding some features and playing with ideas.

This was when I was uncertain as to which libraries to use for making games and what got me to choose [Phaser](http://phaser.io).
The title is mangled English+Portuguese for "Flap Me".

It was hosted on [Dropbox](https://www.dropbox.com/) but they have since altered the hosting feature so I decided I would instead update to Generator-Phaser-Plus, adapt the code/files and serve the project publicly.
Using [Apache Cordova](https://cordova.apache.org/) I had also made an Android version.

Since I'd heard of [GitHub pages](https://pages.github.com/), I figured [GitLab](https://gitlab.com/groups/pages) would have something similar and found [this simple guide](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#add-gitlab-ci) showing how to host static pages (including this game).

## [Play the latest version (1.2.0)](https://vimino.gitlab.io/flapame)

### To Do

- [x] Generate the (NPM+GIT) Project
- [x] Adapt the files to the new structure
- [x] Preloader
- [x] Menu
  - [x] Title
  - [x] Ground object/prefab
  - [x] Bird object/prefab
  - [x] Header motion
  - [x] Start option
  - [x] Fullscreen option
  - [x] Mute option
  - [x] Version
  - [x] Cheats
- [x] Play
  - [x] Pipe object/prefab
  - [x] PipeGroup object/prefab
  - [x] Background
  - [x] Pipe Generation
  - [x] Bird
  - [x] Input
  - [x] Game Mechanics
  - [x] Cheats
  - [x] Sounds
  - [x] Game over
  - [x] Scoreboard object/prefab
    - [x] Local storage
    - [x] Highscore
    - [x] Medal
    - [x] Particle effect
    - [x] Start button
    - [x] Back button
- [x] Serve online
- [x] Show logo
- [x] Several Lives
- [x] Dynamic Title
- [x] Several Sprites
- [x] Gave special traits to some birds
- [x] Added a life counter over a Feather
- [x] Scores are now specific to the bird
- [ ] ???
- [ ] Profit!

### Commands

- **./ez-gpp.sh**
A simple bash script that can install Generator-Phaser-Plus, among other things.
Omit the argument (or use '--help') to see the usage instructions.

- **npm install**
Installs all the required packages (according to the package.json file)

- **npm start** or **gulp** (if installed globally)
Builds the files and opens them locally,
updating dynamically whenever changes are saved.

- **npm run dist**
Prepares the game for distribution (saves it to "./public/").

- **npm run clean**
Deletes the build files.

### Important files and folders

- **src/**
The strings (source code) that flap the bird are here.

- **static/assets/**
Contains the media files, "src/assets.js" lists them for ease of use.

- **gulpfile.js/**
Gulp's (the build tool) configuration, this is where things like the output path are defined.

- **.gitlab-ci.yml**
How I automated building and deploying a static website in GitLab.
Based on the one in this [example project](https://gitlab.com/pages/plain-html).

### License

Copyright &copy; 2016, Vítor "VIMinO" Martins, et al.

The source code is licensed under the [MIT License (MIT)](https://opensource.org/licenses/MIT).

The media files (not included in the [tutorial](http://www.codevinsky.com/phaser-2-0-tutorial-flappy-bird-part-1/) are under a [Creative Commons Attribution 4.0 license](https://creativecommons.org/licenses/by/4.0/).
